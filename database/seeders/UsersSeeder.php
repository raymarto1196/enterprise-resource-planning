<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create default users
        $admin = User::create([
            'name' => 'Erp Admin',
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin'),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        $reference_number = 'RF-'.sprintf('%05d', $admin->id); //RF-00001

        $user_profile = [
            'user_id'            => $admin->id,
            'reference_number'   => $reference_number,
            'firstname'          => 'Admin',
            'lastname'           => 'Admin',
            'middlename'         => NULL,
            'department_id'      => '1',
            'position'           => 'Admin',
        ];

        $admin->profile()->create($user_profile);


        // Assign roles to users
        // $admin->assignRole('ADMIN');
    }
}
