<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $results = Article::all();
        
        $this->params = [
            'count'   => $results->count(),
            'results' => $results,
            'message' => 'Resource retrieved successfully'
        ];
        
        return Inertia::render('Article/Article',$this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Article/ArticleCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'title'         => 'required',
            ]);

        $article_data = [
            'title'               => $request->input('title'),
            'article_content'     => $request->input('article_content'),
        ];

        $result = Article::create($article_data);

        $this->params = [
            'message'   => 'Resource created successfully',
            'results'   => $result,
        ];

        // return Redirect::route('articles.index')->with($this->params);
        return Redirect::route('articles.index')->with($this->params);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Article::find($id);
        
        $this->params = [
            'message' => 'Resource retrieved successfully',
            'results' => $result
        ];
        
        return Inertia::render('Article/ArticleEdit',$this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $article_data = [
            'title'               => $request->input('title'),
            'article_content'     => $request->input('article_content'),
        ];

        $result = $article->update($article_data);

        $this->params = [
            'message'   => 'Resource updated successfully',
            'results'   => $result,
        ];

        return Redirect::route('articles.index')->with($this->params);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Article::find($id);
        $result->delete();

        $this->params = [ // added message because i am planning to send this response to vue view
            'count'     => 1,
            'message'   => 'Resource deleted successfully',
        ];

        return redirect()->back()->with($this->params);
    }

    public function up_vote($article){
        $result = Article::find($article);
        $result->votes += 1;
        $result->save();

        $this->params = [ // added message because i am planning to send this response to vue view
            'count'     => 1,
            'message'   => 'Resource updated successfully',
        ];

        return redirect()->back()->with($this->params);
    }

    public function down_vote($article){
        $result = Article::find($article);
        $result->votes -= 1;
        $result->save();

        $this->params = [ // added message because i am planning to send this response to vue view
            'count'     => 1,
            'message'   => 'Resource updated successfully',
        ];

        return redirect()->back()->with($this->params);
    }
}
