<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\UserController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ORIGINAL
// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// });

//by default Inertia::render load the resource Pages Folder as a starting view
Route::get('/', function () {
    return Inertia::render('Auth/Login');
});


Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {return Inertia::render('Dashboard');})->name('dashboard');
    Route::put('users/{user}/change_status', [UserController::class, 'change_status']);
    Route::put('articles/{article}/up_vote', [ArticleController::class, 'up_vote']);
    Route::put('articles/{article}/down_vote', [ArticleController::class, 'down_vote']);

    Route::resource('/users', UserController::class);
    Route::resource('/articles', ArticleController::class);
});
