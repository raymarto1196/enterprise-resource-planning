<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class UserController extends Controller
{
    public function __construct()
    {
        $this->user = Auth::user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = User::all();
        $results->load('profile');
        
        $this->params = [
            'count'   => $results->count(),
            'results' => $results,
            'message' => 'Resource retrieved successfully'
        ];
        
        // echo '<pre>';
        // var_dump($this->params);
        // echo '</pre>';
        // return response()->json($this->params, 200);

        //this render by default the Pages/User/user.vue folder
        return Inertia::render('User/User',$this->params);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return Inertia::render('User/userCreate',['results' => 'wew']);
        return Inertia::render('User/UserCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate(
            [
                'email'         => 'required|email|unique:users',
                'firstname'     => 'required|string',
                'lastname'      => 'required|string',
                'department_id' => 'required',
                'position'      => 'required',
                'password' => [
                    'required',
                    'confirmed',
                    'min:6',              // must be at least 6 characters in length
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                    'regex:/[@$!%*#?&]/', // must contain a special character
                ]
            ]
        );

        $user_data = [
            'email'     => $request->input('email'),
            'password'  => bcrypt($request->input('password')),
            'name'      => $request->input('firstname').' '.$request->input('lastname')
        ];

        $user = User::create($user_data);

        $reference_number = 'RF-'.sprintf('%05d', $user->id); //RF-00001

        $user_profile = [
            'user_id'            => $user->id,
            'reference_number'   => $reference_number,
            'firstname'          => $request->input('firstname'),
            'lastname'           => $request->input('lastname'),
            'middlename'         => $request->input('middlename'),
            'department_id'      => $request->input('department_id'),
            'position'           => $request->input('position'),
        ];

       $result = $user->profile()->create($user_profile);

       $this->params = [
            'message'   => 'Resource created successfully',
            'results'   => $result,
        ];

        // return redirect()->back()->with('message', 'IT WORKS!');
        // return response()->json($response, 201);
        // so by default in vueCreate visit this controller and will encounter a validate error just use it on the props

        // to redirect to index function and redirect to Pages/User/user.vue view file
        return Redirect::route('users.index')->with($this->params);
        // return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $result = User::find($id);
        $result->load('profile');
        
        $this->params = [
            'message' => 'Resource retrieved successfully',
            'results' => $result
        ];
        
        return Inertia::render('User/UserEdit',$this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $password = $request->input('password');
        
        if(!empty($password)){
            $request->validate(
                [
                    'password' => [
                        'required',
                        'confirmed',
                        'min:6',              // must be at least 6 characters in length
                        'regex:/[a-z]/',      // must contain at least one lowercase letter
                        'regex:/[A-Z]/',      // must contain at least one uppercase letter
                        'regex:/[0-9]/',      // must contain at least one digit
                        'regex:/[@$!%*#?&]/', // must contain a special character
                    ]
                ]
            );
        }

        $request->validate(
            [
                'firstname'     => 'required|string',
                'lastname'      => 'required|string',
                'department_id' => 'required',
                'position'      => 'required',
            ]
        );

        $user = ['user_id' => $id];
        
        $profile_data = [
            'firstname'     => $request->input('firstname'),
            'middlename'    => $request->input('middlename'),
            'lastname'      => $request->input('lastname'),
            'department_id' => $request->input('department_id'),
            'position'      => $request->input('position')
        ];
        
        $result = UserProfile::UpdateOrCreate($user, $profile_data);

        if($password){
            $password = bcrypt($password);
            $result->user()->update(['password' => $password ,'name' => $result->firstname.' '.$result->lastname]);
        } else {
            $result->user()->update(['name' => $result->firstname.' '.$result->lastname]);
        }

        $this->params = [
            'count'     => 1,
            'message'   => 'Resource updated successfully',
            'results'   => $result,
        ];

        //with return a true in user.vue $page.props.flash.message from HandleInertiaRequests.php
        return Redirect::route('users.index', $this->params)->with($this->params);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = User::find($id);
        $result->delete();

        $this->params = [ // added message because i am planning to send this response to vue view
            'count'     => 1,
            'message'   => 'Resource deleted successfully',
        ];

        return redirect()->back()->with($this->params);
    }

    public function change_status(Request $request, $user)
    {
        $result = UserProfile::find($user);
        $result->status = !$result->status;
        $result->save();

        $this->params = [ // added message because i am planning to send this response to vue view
            'count'     => 1,
            'message'   => 'Resource updated successfully',
        ];

        return redirect()->back()->with($this->params);
    }
}
