<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\UserController;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// FOR API TESTING PURPOSE 
Route::apiResource('v1/users', UserController::class);
Route::apiResource('v1/articles', ArticleController::class);
Route::put('v1/users/{user}/change_status', [UserController::class, 'change_status']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
